import Vue from 'vue'
import Router from 'vue-router'
import listData from '@/components/dataConsulta/listData'
import newDato from '@/components/dataConsulta/newDato'
import reputacion from '@/components/dataConsulta/reputacion'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/data',
      name: 'listData',
      component: listData
    },
    {
      path: '/',
      name: 'newDato',
      component: newDato
    },
    {
      path: '/reputacion/:tweet',
      name: 'reputacion',
      component: reputacion
    }
  ],
    mode: 'history'
})
